# Deployed MongoDB and Mongo Express into local K8s cluster 

## Table of Contents

- [Project Description](#project-description)

- [Technologies Used](#technologies-used)

- [Steps](#steps)

- [Installation](#installation)

- [Usage](#usage)

- [How to Contribute](#how-to-contribute)

- [Questions](#questions)

- [References](#references)

- [License](#license)


## Project Description

* Setup local k8s cluster with Minikube 

* Deployed MonogDB and MongoExpress with configuration and credentials extracted into ConfigMap and Secret 

## Technologies Used 

* Kubernetes 

* Docker 

* MongoDB 

* Mongo Express 

## Steps 

Step 1: Create a configuration file for mongodb deployment 

[Mongodb configuration](/images/01_creating_mongodb_config_file.png)

Step 2: Create a secret file to store credentials which will be refrenced in deployment configuration for Mongodb 

    touch mongo-secrets.yaml 

[Creating Secret file](/images/02_creating_secret_file.png)

Step 3: encode mongo secret to base64 

    echo -n 'password' | base64

[base64](/images/03_encoding_the_root_credential_base64.png)
[Secret config file](/images/04_created_mongo_secret_file.png)

Step 4: Apply the secret configuration file

    kubectl apply -f mongo.secret.yaml 

[Applied Secret](/images/05_created_k8_secret.png)

Step 5: Reference Secrets in the mongodb deployment configuration 

[Secrets in Deployment file](/images/06_referencing_secret_in_mongodb_config_file.png)


Step 6: Apply deployment configuration file 

    kubectl apply -f mongo.yaml

[Deployment Applied](/images/07_applying_deployment_file.png)
[extra info](/images/07_extra_info.png)

Step 7: Create service configuration for mongodb deployment 

[Mongodb Service](/images/08_created_service_config_file.png)

Step 8: Apply service configuration 

    kubectl apply -f mongo-service.yaml 

[Applied Mongo Service](/images/09_creating_service.png)

Step 9: Verify Service is attached to the right pod by checking the pod IP in which the port is connected to 

    kubectl describe service mongo-service 
    kubectl get pod -o wide 

note: The first command should be used to check the endpoint which is going to be the pod ip and the port in which it is listening at. The second command will be used to fnd out about the pod ip. if the ip of the endpoint of the service is the same the same as the pod ip then it is correct

[Check endpoint](/images/10_verifying_that_service_is_attacched_to_the_right_port_by_checking_endpoint.png)

Step 10: Create a configuration file for mongo express deloyment 

    touch mongo-express.yaml 

[Creating Mongo Express](/images/11_creating_configuration_file_for_mongo_express.png)

Step 11: Create configmap to use for mongodb url or reference for mongodb service 

[ConfigMap](/images/12_creating_config_map_to_use_as_ref_for_mongoe_config_file.png)

Step 12: Reference configmap and secrets in the mongo express deployment 

[Configmap and secret in Mongo Express Deployment](/images/13_referencing_config_map_in_mogoe_config_file.png)

Step 13: Apply Configmap 

    kubectl apply -f mongo-configmap.yaml

[Apply ConfigMap](/images/14_creating_config_map_for_k8)

Step 14: Apply deployment for mongo express 

    kubectl apply -f mongo-express.yaml 

Step 15: Create external service for mongo express deployment to enable access from the browser 

[Mongo Express Service Config](/images/16_adding_external_service_configuration_to_express_config_file.png)

Step 16: Apply Mongo express service 

    kubectl apply -f mongo-express.yaml 

Step 17: Add external ip address for service 

    minikube service mongo-express.yaml 

[Adding external ip](/images/18_assign_external_ip_address.png)

Step 18: Check Mongo Express UI on browser with the new external ip address and port 

    127.0.0.1:59267

[UI on Browser](/images/19_success_UI_on_browser.png)
[Terminal Display](/images/20_terminal_display.png)

## Installation

Run $ brew install minikube 

## Usage 

Run $ kubectl apply -f Mongo-express.yaml 

## How to Contribute

1. Clone the repo using $ git clone git@gitlab.com:omacodes98/deploying-mongodb-and-mongo-express-into-k8s-cluster-local.git

2. Create a new branch $ git checkout -b your name 

3. Make Changes and test 

4. Submit a pull request with description for review



## Questions

Feel free to contact me for further questions via: 

Gitlab: https://gitlab.com/omacodes98

Email: omacodes98@gmail.com

## References

https://gitlab.com/omacodes98/deploying-mongodb-and-mongo-express-into-k8s-cluster-local

## License

The MIT License 

For more informaation you can click the link below:

https://opensource.org/licenses/MIT

Copyright (c) 2022 Omatsola Beji.